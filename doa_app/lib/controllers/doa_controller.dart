import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../models/doa_model.dart';

class DoaController extends GetxController {
  var doaList = <Doa>[].obs;
  final isLoading = true.obs;
  var posts = <dynamic>[].obs;

@override
  void onInit() {
    super.onInit();
    getDoa();
  }

  getDoa() async {
    String doaUrl = "https://doa-doa-api-ahmadramadhan.fly.dev/api";
    final response = await http.get(Uri.parse(doaUrl));

    if (response.statusCode == 200) {
      final List result = jsonDecode(response.body);
      doaList.value = result.map((e) => Doa.fromJson(e)).toList();
      isLoading.value = false;
      update();
    } else {
      Get.snackbar('Error Loading data!',
          'Server responded: ${response.statusCode}:${response.reasonPhrase.toString()}');
    }
  }

  void searchDoa(String data) {
    if (data.isEmpty) {
      posts.value = doaList.toList();
    } else {
      var filteredData = doaList.where((x) => x.doa.toString().toLowerCase().contains(data.toLowerCase())).toList();
      posts.value = filteredData;
    }
  }
}