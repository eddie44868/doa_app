import 'dart:async';
import 'package:doa_app/page/home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 3), () {
      Get.offAll(const Home());
    });

    return Scaffold(
      backgroundColor: const Color.fromARGB(200, 141, 215, 64),
      body: Center(
        child: Image.asset(
          'assets/images/Doa.png',
          height: 170,
          width: 170,
        ),
      ),
    );
  }
}