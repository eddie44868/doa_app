import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/doa_controller.dart';
import 'list_detail.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  DoaController controller = Get.put(DoaController());//
  TextEditingController textController = TextEditingController();
  var count = 0;

  void showSnackBar() {
    Get.snackbar('Notification', 'Page Refresh',
        icon: const Icon(Icons.refresh),
        duration: const Duration(seconds: 2),
        backgroundColor: Colors.green[100],
        snackPosition: SnackPosition.BOTTOM,
        colorText: Colors.grey[900],);
  }

  exitPush() {
    count++;
    if (count == 2) {
      exit(0);
    } else {
      Get.snackbar('Warning!!!', 'Press Back Button Again to Exit',
        icon: const Icon(Icons.warning),
        backgroundColor: Colors.green[100],
        snackPosition: SnackPosition.BOTTOM,
        colorText: Colors.black);
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    final double heightScreen = MediaQuery.of(context).size.height;
    return WillPopScope(
      onWillPop: () {
        return exitPush();
      },
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: Stack(
              children: [
                Image.asset(
                'assets/images/background.png',
                width: widthScreen,
              ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,///untuk meletakkan widget di bottom
                  children: [
                    Container(
                      height: heightScreen/1.4,
                      width: widthScreen,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40)),
                        color: Colors.white,
                      ),
                    )
                  ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 230,),
                    SizedBox(
                      height: 50,
                      child: TextField(
                        controller: textController,
                        cursorHeight: 25,
                        style: const TextStyle(fontSize: 18),
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(bottom: 3),
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40.0),
                                  borderSide: BorderSide.none
                                ),
                            labelText: "Cari...",
                            filled: true,
                            fillColor: Colors.grey[300],
                            prefixIcon: const Icon(Icons.search),
                            suffixIcon: IconButton(
                              onPressed: textController.clear,
                              icon: const Icon(
                                Icons.clear,
                                color: Colors.green,
                              ))
                          ),
                          onChanged: (_) {
                              controller.searchDoa(textController.text);
                          }
                      ),
                    ),
                    const SizedBox(height: 20,),
                    Obx(
                      () => controller.isLoading.value == true
                          ? SizedBox(
                            height: heightScreen/3,
                            child: const Center(
                                child: CircularProgressIndicator(color: Colors.green,)),
                          )//
                    : Expanded( 
                      child: RefreshIndicator(
                        color: Colors.green[200],
                        onRefresh: () async {
                          await Future.delayed(
                            const Duration(milliseconds: 100), () {
                              controller.posts.clear();
                              setState(() {});
                            });
                          showSnackBar();
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 5),
                          child: ListView.builder(
                            physics: const AlwaysScrollableScrollPhysics(),
                              shrinkWrap: false,
                              itemCount: (controller.posts.isNotEmpty) ? controller.posts.length : controller.doaList.length,
                              itemBuilder: (context, index) {
                                return Column(
                                  children: [
                                    const SizedBox(height: 10,),
                                    Container(
                                      height: 70,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.grey[200],
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 10),
                                        child: Row(
                                          children: [
                                            Image.asset(
                                              'assets/images/tatakan.png',
                                              height: 40,
                                              width: 40,
                                            ),
                                            const SizedBox(
                                              width: 50,
                                            ),
                                            InkWell(
                                              onTap: () {
                                                Get.to(() => (controller.posts.isNotEmpty)
                                                  ? ListDetail(
                                                      doa: controller.posts[index].doa,
                                                      ayat: controller.posts[index].ayat,
                                                      latin: controller.posts[index].latin,
                                                      arti: controller.posts[index].artinya)
                                                  : ListDetail(
                                                      doa: controller.doaList[index].doa,
                                                      ayat: controller.doaList[index].ayat,
                                                      latin: controller.doaList[index].latin,
                                                      arti: controller.doaList[index].artinya));
                                              FocusScope.of(context).unfocus();
                                              },
                                              child: SizedBox(
                                                width: 200,
                                                child: Text(
                                                  controller.posts.isNotEmpty ? controller.posts[index].doa : controller.doaList[index].doa,
                                                  softWrap: true,
                                                  style: const TextStyle(
                                                      fontSize: 16, color: Colors.black),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    const SizedBox(height: 5,),
                                  ],
                                );
                              },
                            ),
                        ),
                      ),
                    ),
                   ),
                  ],
                ),
              ),
            ],
            )
          ),
        ),
      ),
    );
  }
}