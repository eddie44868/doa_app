import 'package:flutter/material.dart';

class ListDetail extends StatefulWidget {
  final String doa;
  final String ayat;
  final String latin;
  final String arti;
  const ListDetail({
    Key? key,
    required this.doa,
    required this.ayat,
    required this.latin,
    required this.arti,
  }) : super(key: key);

  @override
  State<ListDetail> createState() => _ListDetailState();
}

class _ListDetailState extends State<ListDetail> {
  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.doa,
          style: const TextStyle(fontSize: 17, fontWeight: FontWeight.w400,),
          maxLines: 2,
          textAlign: TextAlign.center,
        ),
        backgroundColor: const Color.fromARGB(200, 141, 215, 64),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Column(
          children: [
            const SizedBox(height: 15,),
            Container(
              width: widthScreen-20,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: const Color.fromARGB(250, 239, 219, 148),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    const SizedBox(height: 15,),
                    const Text('Doa :', style: TextStyle(fontSize: 17),),
                    const SizedBox(height: 10,),
                    Center(
                      child: Text(
                        widget.ayat,
                        style: const TextStyle(fontSize: 20),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const SizedBox(height: 15,),
                    const Text('Latin :', style: TextStyle(fontSize: 17),),
                    const SizedBox(height: 10,),
                    Text(
                      widget.latin,
                      style: const TextStyle(fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 15,),
                    const Text('Artinya :', style: TextStyle(fontSize: 17),),
                    const SizedBox(height: 10,),
                    Center(
                      child: Container(
                        width: widthScreen-60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: const Color.fromARGB(199, 187, 241, 129),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            widget.arti,
                            style: const TextStyle(fontSize: 15),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 15,),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}